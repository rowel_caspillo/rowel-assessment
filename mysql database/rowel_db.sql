-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 10:09 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rowel_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `address`, `contact`, `account_type`, `email`, `password`, `token`, `created_at`, `updated_at`) VALUES
(3, 'robert', 'lambunao', '0987654321', 'admin', 'robert@gmail.com', '$2y$10$s7zJoKdcaQxF0EYKq09seuQ9U52sWV3VERopeLqR2JvpeZ9xjQJfO', 'ZUKZlXgwy5hpZc8fGg1XPa98ZaJInXPmWn5Q6hqwrIXeIZNrZ9TSoqPBqGX8', '2020-04-16 06:58:21', '2020-04-16 06:58:21'),
(4, 'rowel', 'lambunao', '0987654321', 'user', 'rowel@gmail.com', '$2y$10$xaDUkAUMaWZNOV6/PeITd.4TXQaWyur.noDikRE6VFcuAc.eItxqi', 'HVeBOnP8wqfX21MkDXPdHbdJafwskBIPcYcqt2W8bZjFXO9napFMwQnnnmFO', '2020-04-16 06:59:45', '2020-04-16 06:59:45'),
(5, 'wilson', 'lambunao', '0987654321', 'user', 'wilson@gmail.com', '$2y$10$QzSidre1JmRyG8410uIFQeFKrSABFTxUzQ28KWxnJa1ltzjDBFM4G', 'ksaXiySp9HD2uyX1ViWPlg911w8uw9RC66txi1vcgszSQqMeKwBC8Oh8r4b5', '2020-04-16 07:00:21', '2020-04-16 07:00:21'),
(7, 'anjum', 'iloilo', '0987654321', 'admin', 'anjum@gmail.com', '$2y$10$MpaxbLgzU7KP5RIjoCGyXOOnFHv7R5oVXW808UG8MawcA4vL8YTeq', 'utkYWLfhxPEuRPU4uH2gO8FB77ROUaj1jbU1h4kUXJ6nQO0jm9270moOO2T5', '2020-04-16 23:02:46', '2020-04-16 23:02:46'),
(8, 'armel', 'antique', '0987654321', 'user', 'armel@gmail.com', '$2y$10$sFHB1qwm8fNTAZ.Yz0doYe8MefFnGIadxVE2en9Qc00nEmA1DizN6', 'JfQumi5tZ3n3cmHxyBiUiVbptHsMFJ9YKSnFSI0oFGlLpRHQQFqmSjAzyZOt', '2020-04-16 23:03:22', '2020-04-16 23:03:22'),
(9, 'orvin', 'miagao', '0987654321', 'user', 'orvin@gmail.com', '$2y$10$f4d9CPgX/iCLcE2tHDLH7ugBq9oiQie4qS/NzgsUsJ/6Vc6EWoTa.', 'gg20uIQna9B8BNxRS3CAKAdlilfZXgij1PcpqACB87fEF19xQEiwhJSBvO8G', '2020-04-16 23:03:50', '2020-04-16 23:03:50'),
(10, 'mark', 'iloilo', '0987654321', 'user', 'mark@gmail.com', '$2y$10$KnNYVzymZuDkHzHpC2O9LuceAzZEybMvjyNpWm/VcmVseMvTNYsE6', '68Dil1WlaV07PeLOXVjB2Ges8WBmuGSSx3f4HJteSYTr7rjLZsLbq60z0Y12', '2020-04-16 23:04:09', '2020-04-16 23:04:09'),
(11, 'jose', 'bacolod', '0987654321', 'user', 'jose@gmail.com', '$2y$10$OMdDfTdOoNDMrZDnUWHTiutkAopLCRC1h4F.FHg07neFXgNrTpO2C', 'tSJHXt3nRTdzCTYVKRlvDxoRvsHOu9G2rwsMQJOHXMVt2KF4rvJs5EK6ffaf', '2020-04-16 23:04:29', '2020-04-16 23:04:29'),
(12, 'karl', 'antique', '0987654321', 'user', 'karl@gmail.com', '$2y$10$xsjmHqob5/WcbHxRg16EZuhTIXdJis8E6MJFfWMVbs31avt7oXCZ2', '45ZkgrsurNKvk3Kv2WZX4EsypRWntgeP4X8kXYhhOmFJunwRGY1oCMqsp5iW', '2020-04-16 23:04:54', '2020-04-16 23:04:54'),
(13, 'ronald', 'bacolod', '0987654321', 'user', 'ronald@gmail.com', '$2y$10$BXkaUigXqk2KSBZShc6zEOkCy7FetREmxbZjCrEftklOMC6O5agz2', 'xrasgS24j7ugIV00jjyiYHvNrWdtS8Cl1wqesuumTRGu1HDttmGmGqwTd1Yx', '2020-04-16 23:05:28', '2020-04-16 23:05:28'),
(14, 'rudy', 'bacolod', '0987654321', 'user', 'rudy@gmail.com', '$2y$10$WKkeeHu7osTBC34jDGdAweidfmABLWreKuu1QTigOYDVvCT1e9u12', 'RHLyM2encn5lioSIY5girNiif4YRTJuPR5oxQoTjfHNQguIpiN0uCvIYnIJ3', '2020-04-16 23:05:43', '2020-04-16 23:05:43'),
(15, 'mac', 'iloilo', '0987654321', 'user', 'mac@gmail.com', '$2y$10$NLYtOf1WFEXvQfYO8LEbd.gEIYPbx6KjaOruF6RcyG9rerDTbFL0G', 'zVFqm3bO7pVHZlTJXZJRqfcoLREz9sVlTwfe1xQajpNqIndF1gQ9hYv1ol4x', '2020-04-16 23:06:04', '2020-04-16 23:06:04'),
(16, 'jaspher', 'cabatuan', '0987654321', 'user', 'jaspher@gmail.com', '$2y$10$0PuEOYN73M4PimA5Cq92QufnntbOYrzhs57wMnDjnNKSXYDpauFxa', 'dAqFLKtkMdj65csDpf5hU1uIgQJkHhR7LKwMnal4Ag20YV8FY94DKo8pMMRZ', '2020-04-16 23:06:36', '2020-04-16 23:06:36'),
(17, 'dan', 'iloilo', '0987654321', 'user', 'dan@gmail.com', '$2y$10$JrVXI6zC/wg3yXZU1cCAK.MVrSQghEDwBeCCA0h0lZF5UQYrThiD2', 'Q0L3nqzzXwcqXa5xDchecXZELuBKvw8kNOZSfDRUvntqqihAJl86rTKAlI5y', '2020-04-17 00:06:23', '2020-04-17 00:06:23'),
(18, 'bill', 'iloilo', '0987654321', 'user', 'bill@gmail.com', '$2y$10$YxlE66o9laXxVSjQr64Ud.Alo8FIymUuv99qpmhfZOcDTdlphxOoS', 'ebHTMrQ2iWc1hfburCJEGKUAeXjh6aMRIqq5JMiRA7wtmFysYQ3cIwig053h', '2020-04-17 00:06:50', '2020-04-17 00:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

CREATE TABLE `deliveries` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_04_16_122450_accounts', 2),
(4, '2020_04_16_154026_products', 3),
(5, '2020_04_16_230543_routes', 4),
(6, '2020_04_16_231107_routes', 5),
(7, '2020_04_16_233517_routes', 6),
(8, '2020_04_16_234107_routes', 7),
(9, '2020_04_16_234301_routes', 8),
(10, '2020_04_17_034455_deliveries', 9),
(11, '2020_04_17_072104_delivery', 10),
(12, '2020_04_17_072411_delivery', 11);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Gucci', '2020-04-16 22:05:09', '2020-04-16 22:05:09'),
(3, 'hermes', '2020-04-16 22:05:27', '2020-04-16 22:05:27'),
(4, 'louie vuitton', '2020-04-16 22:12:03', '2020-04-16 22:12:03'),
(5, 'Alexander McQueen', '2020-04-16 22:15:47', '2020-04-16 22:15:47'),
(6, 'Alexander Wang', '2020-04-16 22:16:14', '2020-04-16 22:16:14'),
(7, 'Anya Hindmarch', '2020-04-16 22:16:34', '2020-04-16 22:16:34'),
(8, 'Anya Hindmarch', '2020-04-16 22:16:35', '2020-04-16 22:16:35'),
(9, 'Aquazzura', '2020-04-16 22:16:59', '2020-04-16 22:16:59'),
(10, 'Balenciaga', '2020-04-16 22:17:07', '2020-04-16 22:17:07'),
(11, 'Balmain', '2020-04-16 22:17:17', '2020-04-16 22:17:17'),
(12, 'Bottega Veneta', '2020-04-16 22:17:25', '2020-04-16 22:17:25'),
(13, 'Burberry', '2020-04-16 22:17:35', '2020-04-16 22:17:35'),
(14, 'Bvlgari', '2020-04-16 22:17:45', '2020-04-16 22:17:45'),
(15, 'Cartier', '2020-04-16 22:18:01', '2020-04-16 22:18:01'),
(16, 'Celine', '2020-04-16 22:18:13', '2020-04-16 22:18:13'),
(17, 'Chanel', '2020-04-16 22:18:20', '2020-04-16 22:18:20'),
(18, 'Chloe', '2020-04-16 22:19:05', '2020-04-16 22:19:05'),
(19, 'Christian Louboutin', '2020-04-16 22:19:25', '2020-04-16 22:19:25'),
(20, 'Coach', '2020-04-16 22:19:36', '2020-04-16 22:19:36'),
(21, 'Delvaux', '2020-04-16 22:19:44', '2020-04-16 22:19:44'),
(22, 'Dior', '2020-04-16 22:19:52', '2020-04-16 22:19:52'),
(23, 'Fendi', '2020-04-16 22:20:17', '2020-04-16 22:20:17'),
(24, 'Ferragamo', '2020-04-16 22:20:25', '2020-04-16 22:20:25'),
(25, 'Gianvito Rossi', '2020-04-16 22:20:34', '2020-04-16 22:20:34'),
(26, 'Givenchy', '2020-04-16 22:20:46', '2020-04-16 22:20:46'),
(27, 'Issey Miyake', '2020-04-16 22:20:57', '2020-04-16 22:20:57'),
(28, 'Jil Sander', '2020-04-16 22:21:27', '2020-04-16 22:21:27'),
(29, 'Jacquemus', '2020-04-16 22:21:37', '2020-04-16 22:21:37'),
(30, 'Jimmy Choo', '2020-04-16 22:21:47', '2020-04-16 22:21:47'),
(31, 'JW Anderson', '2020-04-16 22:21:57', '2020-04-16 22:21:57'),
(32, 'Lanvin', '2020-04-16 22:22:07', '2020-04-16 22:22:07'),
(33, 'Loewe', '2020-04-16 22:22:18', '2020-04-16 22:22:18'),
(34, 'Mansur Gavrie', '2020-04-16 22:22:27', '2020-04-16 22:22:27'),
(35, 'Marc Jacobs', '2020-04-16 22:22:39', '2020-04-16 22:22:39'),
(36, 'Margiela', '2020-04-16 22:22:51', '2020-04-16 22:22:51'),
(37, 'Marni', '2020-04-16 22:23:00', '2020-04-16 22:23:00'),
(38, 'Marsell', '2020-04-16 22:23:08', '2020-04-16 22:23:08'),
(39, 'MCM', '2020-04-16 22:23:24', '2020-04-16 22:23:24'),
(40, 'Miu Miu', '2020-04-16 22:23:34', '2020-04-16 22:23:34'),
(41, 'Moschino', '2020-04-16 22:23:53', '2020-04-16 22:23:53'),
(42, 'Niels Peeraer', '2020-04-16 22:24:10', '2020-04-16 22:24:10'),
(43, 'Off White', '2020-04-16 22:24:19', '2020-04-16 22:24:19'),
(44, 'Phillip Lim', '2020-04-16 22:24:34', '2020-04-16 22:24:34'),
(45, 'Pierre Hardy', '2020-04-16 22:24:46', '2020-04-16 22:24:46'),
(46, 'Prada', '2020-04-16 22:24:59', '2020-04-16 22:24:59'),
(47, 'Proenza Schouler', '2020-04-16 22:25:10', '2020-04-16 22:25:10'),
(48, 'Simon Miller', '2020-04-16 22:25:20', '2020-04-16 22:25:20'),
(49, 'Stella McCartney', '2020-04-16 22:25:34', '2020-04-16 22:25:34'),
(50, 'Stella McCartney', '2020-04-16 22:25:40', '2020-04-16 22:25:40'),
(51, 'Tasaki', '2020-04-16 22:25:47', '2020-04-16 22:25:47');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `name`, `delivery_time`, `cost`, `status`, `created_at`, `updated_at`) VALUES
(2, 'A', '1h', '20', 'delivered', '2020-04-16 22:32:48', '2020-04-16 22:32:48'),
(3, 'B', '30mins', '10', 'to deliver', '2020-04-16 22:33:30', '2020-04-16 22:33:30'),
(4, 'C', '2hrs', '40', 'to deliver', '2020-04-16 22:34:10', '2020-04-16 22:34:10'),
(5, 'D', '1hrs', '20', 'to deliver', '2020-04-16 22:34:28', '2020-04-16 22:34:28'),
(6, 'E', '1hrs', '20', 'to deliver', '2020-04-16 22:34:48', '2020-04-16 22:34:48'),
(7, 'F', '2hrs', '40', 'to deliver', '2020-04-16 22:35:02', '2020-04-16 22:35:02'),
(8, 'G', '1hrs', '20', 'to deliver', '2020-04-16 22:35:18', '2020-04-16 22:35:18'),
(9, 'H', '2hrs', '40', 'to deliver', '2020-04-16 22:35:38', '2020-04-16 22:35:38'),
(10, 'I', '30mins', '10', 'to deliver', '2020-04-16 22:36:05', '2020-04-16 22:36:05'),
(11, 'J', '30mins', '10', 'to deliver', '2020-04-16 22:36:18', '2020-04-16 22:36:18'),
(12, 'K', '40mins', '15', 'to deliver', '2020-04-16 22:36:49', '2020-04-16 22:36:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
