<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class routes extends Model
{
    protected $table = 'routes';
    protected $fillable = ['name','delivery_time','cost','status'];
}
