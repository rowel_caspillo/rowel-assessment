<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\products;
class ProductController extends Controller
{
    public function create(Request $request)
    {

        $products = new products();

        $products->name = $request->input('name');

        $products->save();
        return response()->json($products);

    }

    public function updatebyid(Request $request, $id)
    {
        $products = products::find($id);

        $products->name = $request->input('name');

        $products->save();
        return response()->json($products);

    }

    public function deletebyid(Request $request, $id)
    {

        $products = products::find($id);
        $products->delete();
        return response()->json($products);

    }

    public function show()
    {
        $products = products::all();
        return response()->json($products);
    }

    public function showbyid($id)
    {
        $products = products::find($id);
        return response()->json($products);
    }



}
