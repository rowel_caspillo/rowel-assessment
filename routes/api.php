<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//AccountController
Route::post('/account','AccountController@create');
Route::put('/accountupdate/{id}','AccountController@updatebyid');
Route::delete('/accountdelete/{id}','AccountController@deletebyid');
Route::get('/account','AccountController@show');
Route::get('/account/{id}','AccountController@showbyid');
Route::post('loginaccount','AccountController@logs');


//AuthController
Route::post('/login','AuthController@logs');


//ProductController
Route::post('/products','ProductController@create');
Route::put('/productupdate/{id}','ProductController@updatebyid');
Route::delete('/productdelete/{id}','ProductController@deletebyid');
Route::get('/product','ProductController@show');
Route::get('/product/{id}','ProductController@showbyid');


//RouteController
Route::post('/routes','RouteController@create');
Route::put('/routeupdate/{id}','RouteController@updatebyid');
Route::delete('/routedelete/{id}','RouteController@deletebyid');
Route::get('/route','RouteController@show');
Route::get('/route/{id}','RouteController@showbyid');