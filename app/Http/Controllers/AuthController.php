<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\accounts;

class AuthController extends Controller
{
    public function logs(Request $request) {
        $credentials = $request->only('email', 'password');
         if (Auth::attempt($credentials)) {
           $accounts = Auth::accounts();
           return response()->json([
             'response'   =>  $accounts,
           ]);
         } else {
           return response()->json([
             'message' => 'User not found',
           ]);
         }
   }


}
