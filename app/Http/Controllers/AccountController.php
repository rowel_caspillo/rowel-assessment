<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\accounts;

class AccountController extends Controller
{
    public function create(Request $request)
    {

        $accounts = new accounts();

        $accounts->name = $request->input('name');
        $accounts->address = $request->input('address');
        $accounts->contact = $request->input('contact');
        $accounts->account_type = $request->input('account_type');
        $accounts->email = $request->input('email');
        $accounts->password = Hash::make($request['password']);
        $accounts->token = Str::random(60);

        $accounts->save();
        return response()->json($accounts);



    }

    public function updatebyid(Request $request, $id)
    {

        $accounts = accounts::find($id);

        $accounts->name = $request->input('name');
        $accounts->address = $request->input('address');
        $accounts->contact = $request->input('contact');
        $accounts->account_type = $request->input('account_type');
        $accounts->email = $request->input('email');
        $accounts->password = $request->input('password');

        $accounts->save();
        return response()->json($accounts);

    }

    public function deletebyid(Request $request, $id)
    {

        $accounts = accounts::find($id);
        $accounts->delete();
        return response()->json($accounts);

    }

    public function show()
    {
        $accounts = accounts::all();
        return response()->json($accounts);
    }

    public function showbyid($id)
    {
        $accounts = accounts::find($id);
        return response()->json($accounts);
    }




}
