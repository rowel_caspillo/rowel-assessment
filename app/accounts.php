<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class accounts extends Model
{
    protected $table = 'accounts';
    protected $fillable = ['name','address','contact','account_type','email','password','token'];
}
