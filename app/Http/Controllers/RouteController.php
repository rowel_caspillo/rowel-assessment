<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\routes;

class RouteController extends Controller
{
    public function create(Request $request)
    {

        $routes = new routes();

        $routes->name = $request->input('name');
        $routes->delivery_time = $request->input('delivery_time');
        $routes->cost = $request->input('cost');
        $routes->status = $request->input('status');

        $routes->save();
        return response()->json($routes);



    }

    public function updatebyid(Request $request, $id)
    {

        $routes = routes::find($id);

        $routes->name = $request->input('name');
        $routes->delivery_time = $request->input('delivery_time');
        $routes->cost = $request->input('cost');
        $routes->status = $request->input('status');

        $routes->save();
        return response()->json($routes);

    }

    public function deletebyid(Request $request, $id)
    {

        $routes = routes::find($id);
        $routes->delete();
        return response()->json($routes);

    }

    public function show()
    {
        $routes = routes::all();
        return response()->json($routes);
    }

    public function showbyid($id)
    {
        $routes = routes::find($id);
        return response()->json($routes);
    }


}
